Matrix daily calendar
=====================

Reads CalDav calendars and sends a Matrix message containing a summary of your
calendar entries for the day.

## Getting started

Create a configuration file.  You can use `calendar.example.cfg` as an example.
The script will look for `calendar.cfg` in the current directory by default,
but you can call it anything you want and use the `-c` option to specify the
filename.

Some CalDav servers allow you to create a per-application password for each
application that accesses your calendars, rather than using your account's main
password.  It is recommended to use this feature wherever possible.  For
example, in NextCloud, this can be done in the "Security" section of your
settings, under "Devices & sessions".

Then set up a mechanism (e.g. cron) to run `daily_calendar` once a day,
probably in the morning.
